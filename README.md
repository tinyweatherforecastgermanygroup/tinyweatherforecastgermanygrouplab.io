# TinyWeatherForecastGermanyGroup.GitLab.io

This GitLab Pages page **redirects** requests to [tinyweatherforecastgermanygroup/index](https://gitlab.com/tinyweatherforecastgermanygroup/index/).

[Tiny Weather Forecast Germany](https://tinyweatherforecastgermanygroup.gitlab.io/index/) is an android open source weather app focused on Germany developped by Pawel Dube ([@Starfish](https://codeberg.org/Starfish)) using [open data](https://opendata.dwd.de/) provided by Deutscher Wetterdienst (DWD).

The DWD is Germany's **national weather agency** (similiar to **N**ational **O**ceanic **a**nd **A**tmospheric **A**dministration ([NOAA](https://www.noaa.gov/about-our-agency)) in the US).

[Tiny Weather Forecast Germany](https://gitlab.com/tinyweatherforecastgermanygroup/index/) integrates the *WeatherSpec* class from the [**Gadgetbridge**](https://codeberg.org/Freeyourgadget/Gadgetbridge/) project to enables it's users to sync weather data with their smart gadgets (smart watches/bands, ...) managed using [Gadgetbridge](https://gadgetbridge.org/).

## License/Copyright and Credits

GNU **G**ENERAL **P**UBLIC **L**ICENSE (**GPLv3**) please see the [`License`](https://gitlab.com/tinyweatherforecastgermanygroup/tinyweatherforecastgermanygroup.gitlab.io/-/blob/3358e215ff1fabe33f5bab0a5669d60ecc99cf12/LICENSE) file for details.

**Copyright** of the *main* project -> **Tiny Weather Forecast Germany** -> Pawel Dube ([@Starfish](https://codeberg.org/Starfish))

This CI/CD script was created by Jean-Luc Tibaux (->[eUgEntOptIc44](https://gitlab.com/eUgEntOptIc44))

## Contributing

* All contributions to **Tiny Weather Forecast Germany** are managed at the 'main' code repository
* Feel free to contribute to this script by opening issues and/or merge requests.
* Please also see the automatically generated **code documentation** of Tiny Weather Forecast Germany [here](https://gitlab.com/tinyweatherforecastgermanygroup/twfg-javadoc).
* For cybersec, privacy and/or copyright related issues regarding this repository please directly contact the maintainer [**Jean-Luc Tibaux**](https://gitlab.com/eUgEntOptIc44)
